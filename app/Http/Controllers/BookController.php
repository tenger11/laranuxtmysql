<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Book;
use App\Http\Resources\Book as BookResource;

class BookController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //calling books from database with pagination
        $book = Book::orderBy('created_at', 'desc')->paginate(5);

        //Return collection of books as a resouce
        return BookResource::collection($book);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $book = $request->isMethod('put') ? Book::findOrFail($request->book_id) : new Book;

        $book->id = $request->input('book_id');
        $book->title = $request->input('title');
        $book->author = $request->input('author');
        $book->desc = $request->input('desc');

        if($book->save()){
            return new BookResource($book);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        // store
        $book = Book::findOrFail($id);
        $book->title = $request->input('title');
        $book->author = $request->input('author');
        $book->desc = $request->input('desc');
        if($book->save()){
            return new BookResource($book);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Get book
        $book = Book::findOrFail($id);

        //return single book as resource.
        return new BookResource($book);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Get book
        $book = Book::findOrFail($id);

        if($book->delete()){
            return new BookResource($book);
        }
    }
}
