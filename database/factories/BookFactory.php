<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(App\Models\Book::class, function (Faker $faker) {
    return [
        'title' => $faker->text(50),
        'author' => $faker->text(50),
        'desc' => $faker->text(100)
    ];
});
